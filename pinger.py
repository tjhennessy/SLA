import platform 
import subprocess
import socket
import validators

class Pinger:
    def __init__(self, target, ping_frequency=3.0):
        self.host = target
        self.ping_frequency = ping_frequency # in seconds
        self.is_valid_address = self.validate_address(self.host)
        if self.is_valid_address:
            print("{} is a valid host".format(self.host))
        # it must be either: valid ipv4, ipv6, or domain name otherwise abort
        # determine if host is valid
        # is host ip or domain name
        # need to ping it three times over some period
        # if it fails run traceroute and report it
    
    def ping(self):
        """
        Returns True if host (str) responds to a ping request.
        Remember that a host may not respond to a ping (ICMP) request
        even if the host name is valid.
        """

        # Option for the number of packets as a function of
        param = '-n' if platform.system().lower() == 'windows' else '-c'

        # Building the command. Ex: "ping -c 1 google.com"
        command = ['ping', param, '1', self.host]

        return subprocess.call(command) == 0
    
    def is_ip(self, address):
        return not address.split('.')[-1].isalpha()
    
    def validate_address(self, host):
        return self.is_valid_ipv4_address(host) or \
               self.is_valid_ipv6_address(host) or \
               r'^(?=.{1,253}$)(?!.*\.\..*)(?!\..*)([a-zA-Z0-9-]{,63}\.){,127}[a-zA-Z0-9-]{1,63}$'
        
    def is_valid_ipv4_address(self, address):
        try:
            socket.inet_pton(socket.AF_INET, address)
        except AttributeError:  # no inet_pton here, sorry
            try:
                socket.inet_aton(address)
            except socket.error:
                return False
            return address.count('.') == 3
        except socket.error:  # not a valid address
            return False

        return True

    def is_valid_ipv6_address(self, address):
        try:
            socket.inet_pton(socket.AF_INET6, address)
        except socket.error:  # not a valid address
            return False
        return True
    

if __name__ == '__main__':
    print("localhost is up == {}".format(Pinger("127.0.0.1").ping()))
    Pinger("www.google.com")
    # add argparse
    # mandatory host
    # optional num times to ping
    # optional frequency in seconds for each ping
    # optional address to report back to 